import { Usuario } from './usuario';

export class UsuariosLista {
    private lista: Usuario[] = [];

    constructor() {

    }

    public agregar(usuario: Usuario) {
        this.lista.push(usuario);
        return usuario;
    }

    public actualizarNombre(id: string, nombre: string) {
        const usuario = this.lista.find(list => list.id === id);
        if (usuario) {
            usuario.nombre = nombre;
        }
    }

    public getLista() {

        return this.lista.filter(lista => lista.nombre !== 'sin-nombre');
    }

    public getUsuario(id: string) {
        return this.lista.find(lista => lista.id === id);
    }

    public getUsuariosEnSala(sala: string) {
        return this.lista.filter(lista => lista.sala === sala);
    }

    public borrarUsuario(id: string) {
        const tempUser = this.getUsuario(id);

        this.lista = this.lista.filter(lista => lista.id !== id);

        return tempUser;
    }
}